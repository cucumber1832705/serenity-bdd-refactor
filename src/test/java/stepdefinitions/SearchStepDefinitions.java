package stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import org.apache.http.HttpStatus;
import starter.HerokuappAPI;

import static net.serenitybdd.rest.SerenityRest.*;
import static org.hamcrest.Matchers.*;

public class SearchStepDefinitions {

    @Steps
    public HerokuappAPI herokuappAPI;

    @When("he calls endpoint {string}")
    public void heCallsEndpoint(String arg0) {
        SerenityRest.given().get(arg0);
    }

    @Then("he sees the results displayed for apple")
    public void heSeesTheResultsDisplayedForApple() {
        restAssuredThat(response -> response.statusCode(200));
    }

    @Then("he sees the results displayed for mango")
    public void heSeesTheResultsDisplayedForMango() {
        restAssuredThat(response -> response.body("title", contains("mango")));
    }

    @Then("he doesn not see the results")
    public void he_Doesn_Not_See_The_Results() {
        restAssuredThat(response -> response.body("error", contains("True")));
    }


    @When("he searches for product {string}")
    public void searchForProduct(String product) throws InterruptedException {
        herokuappAPI.searchProduct(product);
    }

    @Then("the search result is correct for following product {string}")
    public void verifyCorrectProductResult(String product) {
        restAssuredThat(response -> response.statusCode(HttpStatus.SC_OK));
//        restAssuredThat(response ->
//                response.body("title".toLowerCase(), contains(product)));
        restAssuredThat(response ->
                response.body("title", hasItem(product)));

    }


    @Then("no results are displayed")
    public void verifyCorrectErrorIsDisplayd() {
        restAssuredThat(response -> response.body("error", contains("True")));
    }
}
